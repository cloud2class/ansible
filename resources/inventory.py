#!/usr/bin/python
import json
import sys

myhash={}
myhash['webservers'] = ['host-10-23-5-64', 'host-10-23-5-65', 'host-10-23-5-66']
myhash['redis'] = {}
myhash['redis']['hosts'] = ['host-10-23-5-67']
myhash['redis']['vars'] = { 'port': 6379 }

myvars = {}
myvars['host-10-23-5-67'] = { 'port': 6379 }

returnval = "{}"

if sys.argv[1] == '--list':
    returnval = json.dumps(myhash)
if sys.argv[1] == '--host':
    try:
        returnval = json.dumps(myvars[sys.argv[2]])
    except:
        pass

print returnval

