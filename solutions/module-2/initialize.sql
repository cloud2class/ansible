CREATE DATABASE /*!32312 IF NOT EXISTS*/ `fileservice` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE USER /*!32312 IF NOT EXISTS*/  'check'@'localhost' IDENTIFIED BY 'abc123';
GRANT SELECT ON fileservice.* TO 'check'@'localhost';
USE `fileservice`;
DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` char(30) NOT NULL,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (1,'demofile');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;
