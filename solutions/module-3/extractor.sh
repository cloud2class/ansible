#!/bin/bash

# A simple condition generator script
myarg=$1

echo "The script is starting"

if [[ -z $myarg ]]
then
    echo "failure"
fi
if [[ $myarg == 'launch' ]]
then
    if [[ ! -f /tmp/extract.stamp ]]
    then
        result="state change"
        touch /tmp/extract.stamp
    fi
fi
if [[ $myarg == 'halt' ]]
then
    if [[ -f /tmp/extract.stamp ]]
    then
        result="state change"
        rm -f /tmp/extract.stamp
    fi
fi

echo $result
echo "The script is finishing"
exit 0
